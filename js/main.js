var db = openDatabase('ESO', '1.0', 'my first database', 1000000);

db.transaction(function (tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS preguntas (id unique, text)');
    tx.executeSql('CREATE TABLE IF NOT EXISTS respuestas (id unique, idpre, text, response)');
    tx.executeSql('INSERT INTO preguntas (id, text) VALUES (1, "¿Quien escribio la odisea?")')
    tx.executeSql('INSERT INTO preguntas (id, text) VALUES (2, "¿Cual es el rio más grande del mundo?")')
    tx.executeSql('INSERT INTO preguntas (id, text) VALUES (3, "¿Como se llama la reina del reino unido?")')
    tx.executeSql('INSERT INTO preguntas (id, text) VALUES (4, "¿Cual es la capital de Mongolia?")')
    tx.executeSql('INSERT INTO preguntas (id, text) VALUES (5, "¿En qué continente está Ecuador?")')
    tx.executeSql('INSERT INTO preguntas (id, text) VALUES (6, "¿Dónde se originaron los juegos olímpicos?")')
    tx.executeSql('INSERT INTO preguntas (id, text) VALUES (7, " ¿Cuándo acabó la II Guerra Mundial?")')
    tx.executeSql('INSERT INTO preguntas (id, text) VALUES (8, "¿Quién es el autor de el Quijote?")')
    tx.executeSql('INSERT INTO preguntas (id, text) VALUES (9, "¿Quién pintó “la última cena”?")')
    tx.executeSql('INSERT INTO preguntas (id, text) VALUES (10, "¿Dónde se encuentra la Sagrada Familia?")')
    tx.executeSql('INSERT INTO preguntas (id, text) VALUES (11, "¿Cuál es el disco más vendido de la historia?")')
    tx.executeSql('INSERT INTO preguntas (id, text) VALUES (12, " ¿Cuál es tercer planeta en el sistema solar?")')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (1, 1, "Homero", true)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (2, 1, "Cervantes", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (3, 1, "Sebastian", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (4, 2, "Amazonas", true)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (5, 2, "Medellin", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (6, 2, "Bogotá", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (7, 3, "Isabel II", true)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (8, 3, "Isabel V", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (9, 3, "Maria Alejandra", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (10, 4, "Bridgetown", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (11, 4, "Nasáu", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (12, 4, "Ulan Bator", true)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (13, 5, "Asia", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (14, 5, "America", true)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (15, 5, "Europa", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (16, 6, "Estado Unidos", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (17, 6, "Brazil", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (18, 6, "Grecia", true)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (19, 7, "1845", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (20, 7, "1940", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (21, 7, "1945", true)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (22, 8, "William Blake", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (23, 8, "Miguel de Cervantes", true)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (24, 8, "Tomás de Aquino", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (25, 9, "Salvador Dalí", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (26, 9, "Vincent Van Gogh", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (27, 9, "Leonardo da Vinci", true)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (28, 10, "Barcelona", true)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (29, 10, "Florencia", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (30, 10, "Madrid", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (31, 11, "THE DARK SIDE OF THE MOON de PINK FLOYD", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (32, 11, "Thriller de Michael Jackson", true)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (33, 11, "BACK IN BLACK de AC/DC ", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (34, 12, "Tierra", true)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (35, 12, "Venus", false)')
    tx.executeSql('INSERT INTO respuestas (id, idpre, text, response) VALUES (36, 12, "Marte", false)')
}, (error) => {
    console.log(error)
}, (res) => {
    console.log(res)
})

  let timeSeconds = 0
  let timeClock = 10
  let responseTrue = 0
  let points = 0
  

  function lanzarDados() {
        document.getElementsByClassName("answer")[0].innerHTML = ''
        document.getElementById("question").innerHTML = ''
        let numDados = Math.round(Math.random() * 1) +1;
        responseTrue = 0
        rollDiceWithoutValues(numDados)
        
    }

    function searchQuestion(num){
        db.transaction( function(tx){
            tx.executeSql('SELECT * FROM preguntas WHERE id = ' + num, [], function (tx, results) {
                document.getElementById('question').innerHTML = '<h5>' + results.rows.item(0).text + '</h5>'
            })
        })
    }

    function searchAnserws(numPre){
        db.transaction(function (tx) {
            tx.executeSql('SELECT * FROM respuestas WHERE idpre = ' + numPre, [], function (tx, results) {
                var len = results.rows.length, i;
                console.log(results)
                const checkbox = document.getElementsByClassName("answer")[0]
                for (i = 0; i < len; i++) {
                    checkbox.innerHTML += `<div class="col-md-6"> <a class="item-response" onclick="checkResponse(${results.rows.item(i).response})">` + results.rows.item(i).text + '</a></div>'
                    console.log(results.rows.item(i).text);
                }
            })
        })
    }

    function rollDiceWithoutValues(numDados) {
        const element = document.getElementById('dados');
        const numberOfDice = +numDados
        const options = {
            element, // element to display the animated dice in.
            numberOfDice, // number of dice to use 
            callback: response
        }
        rollADie(options);
    }

    function response(a) {
        let sum = a.reduce(function (a, b) {
            return a + b
        }, 0)

        setTimeout(() => {
            document.getElementById('load-game').style.display = 'none';
            document.getElementById('question').style.display = 'block';
            searchQuestion(sum)
            searchAnserws(sum)
            document.getElementById('responses').style.display = 'flex';
            getTime(res => {
                console.log(res)
            })
        }, 3100)
        console.log(sum)
    }

    function checkResponse(res){
        if(res){
            points += timeSeconds
            alert('Felicitaciones, tus puntos son' + timeSeconds)
        } else {
            alert('Malo' + timeSeconds)
        }

        responseTrue = 1
        document.getElementById('points').innerHTML = points
        document.getElementById('load-game').style.display = 'block';
        console.log(res)
    }

    function getTime(callback){
        let timeleft = timeClock
        var downloadTimer = setInterval(function () {
            document.getElementById("countdown").innerHTML = timeleft;
            timeleft -= 1

            if (responseTrue) {
                clearInterval(downloadTimer)
                return
            }
            if (timeleft <= 0) {

                showError()
                clearInterval(downloadTimer)
            } 
            timeSeconds = timeleft
            callback(timeleft)
        }, 1000);
    }

    function showError(){
        if (!responseTrue) {
            //alert('sos un malo')
        } 
    }
    
